package controller

import (
	"gitlab.com/kpersohn/gitlab-runner-operator/pkg/controller/gitlabrunner"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, gitlabrunner.Add)
}
